// @flow

import React from "react";

import SiteWrapper from "../Layouts/SiteWrapper.react";

function Empty() {
  return <SiteWrapper> </SiteWrapper>;
}

export default Empty;
