import React from "react";
//import io from "socket.io-client";

import Messages from "./ChatRoom/MessagesView.react";
import ChatInput from "./ChatRoom/ChatInput.react";
import NavbarPage from "../Layouts/NavbarPage.react";

import "./ChatRoom/ChatRoom.css";


class ChatRoomPage extends React.Component
{
    constructor(props) {
        super(props);
        // set the initial state of messages so that it is not undefined on load
        this.state = { 
            messages: ['Hello!', 'Hi!','How are you?', 'Hello!', 'Hi', '', '', '',], 
            username: 'James',
        };
        // Connect to the server
        //this.socket = io(config.api).connect();
        //this.socket.on('server:message', message => {
        //    this.addMessage(message);
        //});
    }

    addMessage(message) {
        // Append the message to the component state
        const messages = this.state.messages;
        messages.push(message);
        this.setState({ messages });
    }

    sendHandler(message) {
        const messageObject = {
        username: this.props.username,
        message
        };
        // Emit the message to the server
        this.socket.emit('client:message', messageObject);
        messageObject.fromMe = true;
        this.addMessage(messageObject);
    }

    componentDidUpdate() {
        // get the messagelist container and set the scrollTop to the height of the container
        const objDiv = document.getElementById('messageList');
        objDiv.scrollTop = objDiv.scrollHeight;
    }

    render () {
        return (
            <div>
                <NavbarPage />
                <div className="chatWindow">
                    <Messages messages={this.state.messages} />
                    <ChatInput onSend={this.sendHandler} />
                </div>
            </div>
        )
    }
}

export default ChatRoomPage;