import React from "react";

import {Icon, Button, Grid} from "tabler-react";

import "./ChatRoom.css";

class ChatInput extends React.Component
{
    constructor(props) {
        super(props);
        // Set initial state of the chatInput so that it is not undefined
        this.state = { chatInput: '' };
        // React ES6 does not bind 'this' to event handlers by default
        this.submitHandler = this.submitHandler.bind(this);
        this.textChangeHandler = this.textChangeHandler.bind(this);
    }
    
    textChangeHandler(event)  {
        this.setState({ chatInput: event.target.value });
    }

    submitHandler(event) {
        // Stop the form from refreshing the page on submit
        event.preventDefault();
        // Call the onSend callback with the chatInput message
        this.props.onSend(this.state.chatInput);
        // Clear the input box
        this.setState({ chatInput: '' });
    }

    render() {
        return (
            <div className="chat-input-row">
                <div className="chat-input-upload">
                    <Button className="icon-button" color="pink" icon="image" />
                    <Button className="icon-button" color="pink" icon="video" />
                    
                </div>
                <div className="chat-input-field">
                    <form className="chat-input" onSubmit={this.submitHandler}>

                        <input className="chat-input-input" type="text"
                        onChange={this.textChangeHandler}
                        value={this.state.chatInput}
                        placeholder="Write a message..."
                        required />
                    </form>
                </div>
                <div className="chat-input-send">
                    <Icon className="fa-icon" prefix="fa" name="send" />
                </div>
            </div>
        );
    }
}

export default ChatInput;