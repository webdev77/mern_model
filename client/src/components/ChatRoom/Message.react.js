import React from "react";
import "./ChatRoom.css";

import {Avatar} from "tabler-react";

class Message extends React.Component
{
    render() {
        // Was the message sent by the current user. If so, add a css class
        const fromMe = this.props.fromMe ? 'from-me' : '';
        return (
          <div className={`message ${fromMe}`}>
            <Avatar icon="user" />
            {/*<div className='username'>
              { this.props.username'Anouk Samuel' }
            </div>
            */}
            <label className='message-body'>
              { /*this.props.message*/'Hi! I am Anouk Samuel! asldkfjalskdjfoiqwerukajsdvkzxcvzklxcjvkljsaldfjuoiweuqrifsaldkghkjfghkjashfodiuowiequroeiwrufasdlfj' }   
            </label>
          </div>
        );
    }
}

export default Message;

/* https://www.freecodecamp.org/news/building-a-modern-chat-application-with-react-js-558896622194/ */