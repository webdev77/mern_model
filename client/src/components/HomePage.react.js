// @flow

import * as React from "react";

import {
  Page,
  Avatar,
  Icon,
  Grid,
  Card,
  Text,
  Table,
  Alert,
  Progress,
  colors,
  Dropdown,
  Button,
  StampCard,
  StatsCard,
  ProgressCard,
  Badge,
  Nav,
  BlogCard,
} from "tabler-react";

import {MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBCollapse, MDBNavItem} from "mdbreact";

import Carousel from "nuka-carousel";
import C3Chart from "react-c3js";

import SiteWrapper from "../Layouts/SiteWrapper.react";
import NavbarPage from "../Layouts/NavbarPage.react";


function Home() {
  return (
    <div>
      <NavbarPage></NavbarPage>
      <Grid.Row>
          <Carousel speed={800} slideWidth={0.25} cellSpacing={3}>
            <img src="./demo/header/post-s-1.jpg" />
            <img src="./demo/header/post-s-2.jpg" />
            <img src="./demo/header/post-s-3.jpg" />
            <img src="./demo/header/post-s-4.jpg" />
            <img src="./demo/header/post-s-1.jpg" />
            <img src="./demo/header/post-s-2.jpg" />
          </Carousel>
      </Grid.Row>
      <Page.Content>
        <Grid.Row width={15}>
        </Grid.Row>
        <Grid.Row>
          <Grid.Col width={12}>
            <Card>
              <Table
                responsive
                highlightRowOnHover
                hasOutline
                verticalAlign="center"
                cards
                className="text-nowrap"
              >
                <Table.Header>
                  <Table.Row>
                    <Table.ColHeader alignContent="center" className="w-1">
                      <i className="icon-people" />
                    </Table.ColHeader>
                    <Table.ColHeader>User</Table.ColHeader>
                    <Table.ColHeader>Usage</Table.ColHeader>
                    <Table.ColHeader alignContent="center">
                      Payment
                    </Table.ColHeader>
                    <Table.ColHeader>Activity</Table.ColHeader>
                    <Table.ColHeader alignContent="center">
                      Satisfaction
                    </Table.ColHeader>
                    <Table.ColHeader alignContent="center">
                      <i className="icon-settings" />
                    </Table.ColHeader>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Col alignContent="center">
                      <Avatar
                        imageURL="demo/faces/female/26.jpg"
                        className="d-block"
                        status="green"
                      />
                    </Table.Col>
                    <Table.Col>
                      <div>Elizabeth Martin</div>
                      <Text size="sm" muted>
                        Registered: Mar 19, 2018
                      </Text>
                    </Table.Col>
                    <Table.Col>
                      <div className="clearfix">
                        <div className="float-left">
                          <strong>42%</strong>
                        </div>
                        <div className="float-right">
                          <Text.Small muted>
                            Jun 11, 2015 - Jul 10, 2015
                          </Text.Small>
                        </div>
                      </div>
                      <Progress size="xs">
                        <Progress.Bar color="yellow" width={42} />
                      </Progress>
                    </Table.Col>
                    <Table.Col alignContent="center">
                      <Icon payment name="visa" />
                    </Table.Col>
                    <Table.Col>
                      <Text size="sm" muted>
                        Last login
                      </Text>
                      <div>4 minutes ago</div>
                    </Table.Col>
                    <Table.Col alignContent="center">42%</Table.Col>
                    <Table.Col alignContent="center">
                      <Dropdown
                        trigger={
                          <Dropdown.Trigger
                            icon="more-vertical"
                            toggle={false}
                          />
                        }
                        position="right"
                        items={
                          <React.Fragment>
                            <Dropdown.Item icon="tag">Action </Dropdown.Item>
                            <Dropdown.Item icon="edit-2">
                              Another action{" "}
                            </Dropdown.Item>
                            <Dropdown.Item icon="message-square">
                              Something else here
                            </Dropdown.Item>
                            <Dropdown.ItemDivider />
                            <Dropdown.Item icon="link">
                              {" "}
                              Separated link
                            </Dropdown.Item>
                          </React.Fragment>
                        }
                      />
                    </Table.Col>
                  </Table.Row>
                </Table.Body>
              </Table>
            </Card>
          </Grid.Col>
        </Grid.Row>
        <Grid.Row width={15}>
          <Grid.Col lg={9}>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
            <BlogCard></BlogCard>
          </Grid.Col>
          <Grid.Col>
            <Grid.Row>
              <Text center="true" size={50}>
                <strong>Hey Babies! I will be uploading sexy, exclusive content daily here for you guys only!</strong>
                <br></br>
                Behind the scenes video and pictures from different shoots and other content that is way too sexy for Instagram.
              </Text>
            </Grid.Row>
          </Grid.Col>
        </Grid.Row>
      </Page.Content>
    </div>
  );
}

export default Home;
