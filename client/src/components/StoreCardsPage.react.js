// @flow

import * as React from "react";

import { Page, Grid, Table, Card, Badge } from "tabler-react";
import StoreCard from "../cards/StoreCard.react";

import NavbarPage from "../Layouts/NavbarPage.react";

function StoreCardsPage(): React.Node {
  return (
    <div>
      <NavbarPage></NavbarPage>
      <Page.Content title="Shop">
        <Grid.Row>
          <Grid.Col lg={4}>
            <StoreCard
              title="Apple iPhone 7 "
              subtitle="Each item has been worn by me in either pohoshoots, on a daily basis, or both. Every purchase comes with a personalized note from Anouk."
              price="$499"
              imgUrl="demo/header/post-s-1.jpg"
            />
          </Grid.Col>

          <Grid.Col lg={4}>
            <StoreCard
              title="GoPro HERO6 Black"
              subtitle="Each item has been worn by me in either pohoshoots, on a daily basis, or both. Every purchase comes with a personalized note from Anouk."
              price="$599"
              imgUrl="https://tabler.github.io/tabler/demo/products/gopro-hero.jpg"
            />
          </Grid.Col>

          <Grid.Col lg={4}>
            <StoreCard
              title="GoPro HERO6 Black"
              subtitle="Each item has been worn by me in either pohoshoots, on a daily basis, or both. Every purchase comes with a personalized note from Anouk."
              price="$599"
              imgUrl="demo/header/post-s-4.jpg"
            />
          </Grid.Col>

          <Grid.Col lg={4}>
            <StoreCard
              title="GoPro HERO6 Black"
              subtitle="Each item has been worn by me in either pohoshoots, on a daily basis, or both. Every purchase comes with a personalized note from Anouk."
              price="$599"
              imgUrl="demo/header/post-s-2.jpg"
            />
          </Grid.Col>
          
        </Grid.Row>

        
      </Page.Content>
    </div>
  );
}

export default StoreCardsPage;
