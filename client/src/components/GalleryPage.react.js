// @flow

import * as React from "react";

import { Page, Grid, GalleryCard, Form } from "tabler-react";

import SiteWrapper from "../Layouts/SiteWrapper.react";
import NavbarPage from "../Layouts/NavbarPage.react";
import Gallery from "react-grid-gallery";

import json from "../data/Gallery.Items";
// TODO:Add GalleryCardList component to avoid insert extra className
// TODO:Update Page.Header to additional components

function realImgDimension(imgSrc) {
  var img = new Image();
  img.src = imgSrc;
  return {
    width: img.width,
    height: img.height
  };
}

const IMAGES =
[{
        src: "demo/header/post-s-1.jpg",
        thumbnail: "demo/header/post-s-1.jpg",
        thumbnailWidth: realImgDimension("demo/header/post-s-1.jpg").width,
        thumbnailHeight: realImgDimension("demo/header/post-s-1.jpg").height,
        caption: "Girl"
},
{
        src: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_b.jpg",
        thumbnail: "https://c2.staticflickr.com/9/8356/28897120681_3b2c0f43e0_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212,
        caption: "Boats (Jeshu John - designerspics.com)"
},

{
        src: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_b.jpg",
        thumbnail: "https://c4.staticflickr.com/9/8887/28897124891_98c4fdd82b_n.jpg",
        thumbnailWidth: 320,
        thumbnailHeight: 212
},


{
        src: "demo/photos/nathan-anderson-316188-500.jpg",
        thumbnail: "demo/photos/nathan-anderson-316188-500.jpg",
        thumbnailWidth: realImgDimension("demo/photos/nathan-anderson-316188-500.jpg").width,
        thumbnailHeight: realImgDimension("demo/photos/nathan-anderson-316188-500.jpg").height,
        caption: "Girl"
},

{
  src: "demo/header/post-s-4.jpg",
  thumbnail: "demo/header/post-s-4.jpg",
  thumbnailWidth: realImgDimension("demo/header/post-s-1.jpg").width,
  thumbnailHeight: realImgDimension("demo/header/post-s-1.jpg").height,
  caption: "Girl"
},


{
  src: "demo/header/post-s-3.jpg",
  thumbnail: "demo/header/post-s-3.jpg",
  thumbnailWidth: realImgDimension("demo/header/post-s-3.jpg").width,
  thumbnailHeight: realImgDimension("demo/header/post-s-3.jpg").height,
  caption: "Girl"
},

{
  src: "demo/header/blog-2.jpg",
  thumbnail: "demo/header/blog-2.jpg",
  thumbnailWidth: realImgDimension("demo/header/blog-2.jpg").width,
  thumbnailHeight: realImgDimension("demo/header/blog-2.jpg").height,
  caption: "Girl"
},

{
  src: "demo/photos/josh-calabrese-66153-500.jpg",
  thumbnail: "demo/photos/josh-calabrese-66153-500.jpg",
  thumbnailWidth: realImgDimension("demo/photos/josh-calabrese-66153-500.jpg").width,
  thumbnailHeight: realImgDimension("demo/photos/josh-calabrese-66153-500.jpg").height,
  caption: "Girl"
},

{
  src: "demo/photos/jeremy-bishop-330225-500.jpg",
  thumbnail: "demo/photos/jeremy-bishop-330225-500.jpg",
  thumbnailWidth: realImgDimension("demo/photos/jeremy-bishop-330225-500.jpg").width,
  thumbnailHeight: realImgDimension("demo/photos/jeremy-bishop-330225-500.jpg").height,
  caption: "Girl"
},

{
  src: "demo/header/post-s-2.jpg",
  thumbnail: "demo/header/post-s-2.jpg",
  thumbnailWidth: realImgDimension("demo/header/post-s-2.jpg").width,
  thumbnailHeight: realImgDimension("demo/header/post-s-2.jpg").height,
  caption: "Girl"
},

{
  src: "demo/photos/jonatan-pie-226191-500.jpg",
  thumbnail: "demo/photos/jonatan-pie-226191-500.jpg",
  thumbnailWidth: realImgDimension("demo/photos/jonatan-pie-226191-500.jpg").width,
  thumbnailHeight: realImgDimension("demo/photos/jonatan-pie-226191-500.jpg").height,
  caption: "Girl"
},
]

function GalleryPage(): React.Node {
  const options = (
    <React.Fragment>
      <Form.Select className="w-auto mr-2">
        <option value="asc">Newest</option>
        <option value="desc">Oldest</option>
      </Form.Select>
      <Form.Input icon="search" placeholder="Search photo" />
    </React.Fragment>
  );
  return (
    <div>
      <NavbarPage></NavbarPage>
      <Page.Content>
        <Page.Header
          title="Gallery"
          subTitle="1 - 12 of 1713 photos"
          options={options}
        />


        <Gallery images={IMAGES} backdropClosesModal={true}/>
        {/*
        <Grid.Row className="row-cards">
          {json.items.map((item, key) => (
            <Grid.Col sm={6} lg={4} key={key}>
              <GalleryCard>
                <GalleryCard.Image
                  src={item.imageURL}
                  alt={`Photo by ${item.fullName}`}
                />
                <GalleryCard.Footer>
                  <GalleryCard.Details
                    avatarURL={item.avatarURL}
                    fullName={item.fullName}
                    dateString={item.dateString}
                  />
                  <GalleryCard.IconGroup>
                    <GalleryCard.IconItem name="eye" label={item.totalView} />
                    <GalleryCard.IconItem
                      name="heart"
                      label={item.totalLike}
                      right
                    />
                  </GalleryCard.IconGroup>
                </GalleryCard.Footer>
              </GalleryCard>
            </Grid.Col>
          ))}
        </Grid.Row>
          */}
      </Page.Content>
    </div>
  );
}

export default GalleryPage;
