import * as React from "react";

import NavbarPage from "../Layouts/NavbarPage.react";
import {Page, Card, Grid, Button, Text, Header, Form} from "tabler-react";
import Img from "react-image";
import { from } from "rxjs";


function Donation(): React.Node
{
    return (
        <div>
            <NavbarPage />
            <Page.Content >
                <Card>
                    <Card.Body>
                        <Grid.Row lg={6}>
                            <Img src={'demo/header/header-1.jpg'}/>
                        </Grid.Row>
                        <div style={{marginLeft: 200, marginRight: 200}}>
                            <Header.H3>
                                <Text center="true" size="sm" style={{marginTop: 50}}>
                                    Choose amount to donate
                                </Text>
                            </Header.H3>
                                    
                            <Button.List align="center">
                                <Grid.Row>
                                <Grid.Col>
                                <Button color="outline-pink" block>$25</Button>
                                </Grid.Col>
                                <Grid.Col>
                                    <Button color="outline-pink" block>$50</Button>
                                </Grid.Col>
                                <Grid.Col>
                                    <Button color="outline-pink" block>$75</Button>
                                </Grid.Col>
                                <Grid.Col>
                                    <Button color="outline-gray" block>$ Other</Button>
                                </Grid.Col>
                                </Grid.Row>
                            </Button.List>

                            <div style={{marginLeft: 'auto', marginRight: 'auto', marginTop: 50}}>
                                <Button color="pink" size="lg" block>Donate</Button>
                            </div>
                            <div>
                            <Form.Checkbox 
                                name="Monthly Donation"
                                label="Make this monthly donation"
                                value="donation" />
                            </div>
                        </div>
                    </Card.Body>
                </Card>
            </Page.Content>
        </div>
    );
}

export default Donation;